document.getElementById("back-to-top").addEventListener("click", e => {
  window.scroll({top: 0, behavior: "smooth"});
  e.preventDefault();
});
document.querySelectorAll(".future").forEach(
  post => {post.dataset.pubdate <= new Date().toISOString()
    && post.classList.remove("future");}
);
document.querySelectorAll("h3, h4, h5, h6").forEach(
  h => {h.innerHTML += ' <a href="#' + h.id + '" class="anchor-link">#</a>';});
