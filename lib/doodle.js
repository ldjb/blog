if (location.hash == "#hzfpb28qf59e7gfb") {
    history.replaceState({}, "", "/doodle/hzfpb28qf59e7gfb");
    fetch("/doodle/hzfpb28qf59e7gfb/" + new Date().getTime() + ".json").then(
      d => d.json().then(
        j => {
          populateResults(j);}));
}
else {
    document.write("hash not provided");
}

function populateResults(data) {
  updated.innerHTML = "Updated: " + new Date(data.latestChange);
  let arrResults = [];
  for (let option in data.options) {
    let votes = 0;
    for (let participant in data.participants) {
        votes += data.participants[participant].preferences[option];
    }
    arrResults.push([votes, '<tr><td class="title">'
      + data.options[option].text
      + '</td><td title="'
      + (votes / data.participantsCount * 100).toFixed(1)
      + '%"><div style="width: '
      + votes / data.participantsCount * 100
      + '%; background: #66f; color: '
      + (votes > 0 ? "#fff" : "#000")
      + '; text-align: center;">'
      + votes
      + '</div></td></tr>']);
  }
  arrResults.sort((a,b) => b[0]-a[0]);
  arrResults.forEach(result => {
    results.innerHTML += result[1];
  });
}