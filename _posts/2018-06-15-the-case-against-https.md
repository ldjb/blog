---
layout: post
title: The case against HTTPS
tags: [technology]
excerpt_separator: <!-- more -->
---

I recently read [a blog post](https://scotthelme.co.uk/https-anti-vaxxers/) dispelling arguments against HTTPS. I am certainly of the opinion that 99% of websites should use an encrypted connection. However, I do think critics of HTTPS do have some good points, and there are cases where an unencrypted connection is actually desirable.

<!-- more -->

### Criticism of Google

One criticism highlighted in the above blog post is against Google. Whilst Google are certainly not deprecating HTTPS, they have been [prioritising websites served under HTTPS](https://webmasters.googleblog.com/2014/08/https-as-ranking-signal.html) in web search rankings for a few years now. This means that a website may be penalised slightly for not adopting HTTPS. Whilst there are good reasons for this, it does mean a more useful website may not rank as high as a less useful one, just because it is not served over an encrypted connection. In this instance, I think Google have made the right decision, but I accept that not everyone will see it that way. Certainly, there is a point of view that says Google should provide the user with the most relevant results, regardless of whether a website uses HTTPS or not. And I think there is a valid point there. However, for me, security is more important.

### Slow and expensive?

Another argument against HTTPS brought up in the linked blog post is that HTTPS is "slow and expensive". Whilst I wouldn't describe it with those terms, HTTPS is certainly slower and computationally expensive than using an unencrypted connection. It also results in larger amounts of data sent over the wire.

Once again, security should probably take priority. But it is always worth considering people with poor Internet connectivity (if they live in a remote area, for example) and low-power devices. Security aside, using HTTPS may result in a poorer user experience.

### Security issues with HTTPS

HTTPS is not perfect. There are flaws with the system, and I don't think these should be completely dismissed. For instance, there were [a large number of issues](https://wiki.mozilla.org/CA:Symantec_Issues) in how Symantec handled security. These were so severe that many web browser vendors ended up distrusting Symantec certificates.

Unfortunately, despite these issues, HTTPS is the best solution we have. So it is still worth using. But that doesn't mean any security concerns are invalid.

### Learning about HTTP

Before learning about HTTPS, it is essentially necessary to learn about HTTP. To understand how the protocol works, there is no substitute to making some actual HTTP requests yourself.

You can do this by opening up a TCP/IP connect with `telnet`:

```
$ telnet example.com 80
Trying 93.184.216.34...
Connected to example.com.
Escape character is '^]'.
GET / HTTP/1.1
Host: example.com

HTTP/1.1 200 OK
Cache-Control: max-age=604800
Content-Type: text/html
Date: Mon, 11 Jun 2018 16:39:05 GMT
Etag: "1541025663+ident"
Expires: Mon, 18 Jun 2018 16:39:05 GMT
Last-Modified: Fri, 09 Aug 2013 23:54:35 GMT
Server: ECS (lga/13A2)
Vary: Accept-Encoding
X-Cache: HIT
Content-Length: 1270

<!doctype html>
[...]
```
{:.console}

This is a fantastic way to learn how HTTP works. But you can't do this over HTTPS without figuring out how to deal with TLS. Sure, you can construct HTTPS requests using `curl`, but it's not the same as actually typing out the headers manually over a live TCP/IP connection.

Therefore, it is useful to have some websites available over HTTP (example.com is a good one) so you can learn how the protocol works.

### Demonstrating the need for HTTPS

A fantastic way to show someone the issues of using an unencrypted connection (and thus why HTTPS is so important) is to demonstrate it to them. Actually stage a man-in-the-middle attack and show how a requests can be hijacked. Naturally, you can't do this over an encrypted connection, so the website being used to demonstrate this must be served over regular HTTP.

Without any sites using an unencrypted connection, people would probably question the necessity of HTTPS. So it's worth having some websites that do not use HTTPS to demonstrate its purpose.

### localhost

When I'm developing a website, I'll very often have a version of the site running locally on my computer so I can test it. This saves having to constantly upload files to a remote server. In fact, as I'm typing this post now, I have a copy of my website being served from `localhost`. That way I know how this web page will look when it is published to the actual website.

Serving websites over HTTPS on `localhost` is cumbersome and serves little purpose. If a man-in-the-middle attack is possible on my own computer, the security of my test website is the least of my worries.

A similar thing can be said for websites only available on an internal network. Whilst there is a greater security risk should you be using a public Wi-Fi network, for example, my home network is only used by myself and people I trust. Therefore, serving websites over HTTPS is unnecessary. (Of course, there is always the possibility that a device on the network has been compromised. Depending on the nature of the website, HTTPS may still be useful. However, if you're simply serving a test website to see how it looks and functions on different devices, it shouldn't really matter.)

### Connecting to a public Wi-Fi network

A lot of public Wi-Fi networks use a [captive portal](https://en.wikipedia.org/wiki/Captive_portal) that requires the user to sign in or accept some terms and conditions before they can connect to the Internet.

To get to the captive portal, you need to make an HTTP request, which will be intercepted, and the captive portal will be served. This is, in fact, a man-in-the-middle attack. Naturally, HTTPS prevents this from working. Enter websites like [NeverSSL](http://neverssl.com/) which claims it will never be served over HTTPS. Therefore, if you wish to connect to a network that uses a captive portal, you can simply attempt to access the NeverSSL website, and you will be taken to the captive portal. If NeverSSL were served over HTTPS, this would not work, and the website's purpose would be defeated.

### APIs for legacy clients

Suppose your company has previously produced Internet-connected devices that interact with an API over HTTP. They expect an unencrypted connection and it is not possible to upgrade the connection to HTTPS. If you were to disable unencrypted connections, these devices would cease to function and your customers would be unhappy. Whilst all future devices should support and use HTTPS, it may make sense to keep functioning devices that are already in use. As such, retaining a legacy API for those devices may be sensible.

### Conclusion

And there you have it. Whilst I believe the vast majority of websites should use HTTPS, that's not to say it's unfair to make criticisms about HTTPS or that there aren't legitimate reasons for serving a website over an unencrypted connection.

There are further concerns that could be made about how web browsers are indicating (or not) the use of an encrypted connection to users, but that will have to be a post for another day.