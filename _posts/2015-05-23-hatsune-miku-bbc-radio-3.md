---
layout: post
title: Hatsune Miku on BBC Radio 3
tags: [music, Japan]
imported: true
---

As part of [the 9th November episode of *Private Passions*](https://www.bbc.co.uk/programmes/b04nql09) on BBC Radio 3, a rendition of Finnish song “Ievan Polkka” featuring synthesised vocals created using the Japanese Hatsune Miku Vocaloid software was broadcast.

Amusingly, neither the presenter nor the guest, Roger Law, knew what the track was. It was listed in the BBC Music database as “[Unidentified chinese music for the car](https://www.bbc.co.uk/music/tracks/n3nd8j)”.

Here is a recording I made of that section of the programme:

<audio src="/media/2015-05-23-hatsune-miku-bbc-radio-3.mp3" controls></audio>
