---
layout: page
title: Doodle poll results
index: false
---

**Social watching: Q3 2018 anime**

<p><a href="https://doodle.com/poll/hzfpb28qf59e7gfb">Vote on Doodle</a></p>

<table id="results" border="1" style="width: 100%; border: 1px;"></table>
<p id="updated"></p>
<script src="/lib/doodle.js"></script>

<style>
td.title {
  width: 50%;
}
@media screen and (min-width: 640px) {
  td.title {
    width: 0;
    white-space: nowrap;
  }
}
td {
    border: 1px solid black;
}
</style>